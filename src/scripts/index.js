import '../styles/index.scss';

$(document).ready(()=>{
    
    $(".fancybox").fancybox({
        openEffect  :'none',
        closeEffect :'none'
    });

    console.log('Hello jQuery');

    /**
     * Documentation for the slider
     * https://bxslider.com/examples/auto-show-start-stop-controls/ 
     */ 

     /**
     * Documentation for the Lightbox - Fancybox
     * See the section 5. Fire plugin using jQuery selector.
     * http://fancybox.net/howto
     */ 

     /**
      * Boostrap Modal (For the Learn More button)
      * https://getbootstrap.com/docs/4.0/components/modal/
      */
    

});

/**
 * REMEMBER!!
 * Declaring Global functions that are accessible in your HTML.
 */ 
window.helloWorld = function() {
    console.log('HEllooOooOOo!');
};

var acc = document.getElementsByClassName("acc-but");
var i;

for(i = 0; i < acc.length; i++){
    acc[i].addEventListener("click",function(){
        this.classList.toggle("active");

        var panel = this.nextElementSibling;
        if(panel.style.display === "block"){
            panel.style.display = "none";
        }else{
            panel.style.display = "block";
        }
    });
}
/*
$(function(){
    $('.bxslider').bxSlider({
        mode: 'fade',
        captions: false,
        slideWidth: 600
    });
});
*/